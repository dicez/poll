# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist as DoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.forms import BaseModelFormSet
from django.forms import ModelForm,DateInput,DateField, Textarea,EmailField
from django.forms import modelformset_factory
from django.utils.translation import ugettext_lazy as _
from models import Person, Pool, VOTE, Choice,Question
from dateutil.parser import parse
DATE_INPUT_FORMATS=('%d-%m-%Y','%d/%m/%y','%Y/%m/%d','%d%m%Y','%d%m%y')


class PersonForm(ModelForm):

    class Meta:
        model=Person
        TST=('%d-%m-%Y','%d/%m/%Y','%Y/%m/%d')
        #exclude=['birth']
        fields='__all__'#('name','email','cell','birth','transport','accept_spam','accept_wzap')
        birth=DateField(input_formats=DATE_INPUT_FORMATS)
        labels={
            'name':_('nome'),
            'email':_('E-mail'),
            'cell':_('telefone'),
            'birth':_('data de nascimento'),
            'transport':_('meio de transporte para chegar a dicez'),
            'accept_spam':_('aceito receber e-mail de promoções da DiceZ'),
            'accept_wzap':_('aceito receber sms e contato por WhatsApp de promoções da DiceZ'),
        }
        
@csrf_exempt
def poll_html(pool,request,p):
    if request.method == 'POST':
        form= request.POST
        try:
            p=Person.objects.get(id=p)
        except DoesNotExist as e:
            print e
            return redirect('/cadastro/')
        
        for choice in form.iteritems():
            c=None
            t=None
            m=None
            if choice[0] not in ['person','csrfmiddlewaretoken']:
                q=Question.objects.get(id=int(choice[0]))
                
                if q.question_type=='S':
                    c=int(choice[1])
                if q.question_type=='T':
                    t=choice[1]
                if q.question_type=='M':
                    m=request.POST.getlist(choice[0])
                     
                if q.question_type=='A':
                    print choice[0]
                    t=form[choice[0]]
                    m=request.POST.getlist(choice[0])
                    print t, m, form['4']
                Choice().create(q,p,c,t,m)
            po=Pool.objects.filter(id__gt=p.id, active=True)
        if po:
            return redirect('/pesquisa/'+str(p[0].id))
        return redirect('/grato')
    questions = pool.question_set.all()
    quests=[]
    count =0
    for q in questions:
        min_value=''
        max_value=''
        if q.min_value:
            min_value=q.min_value
        if q.max_value:
            max_value=q.max_value
        quests.append({'question_text':q.question_text,'id':q.id, 'min_value':min_value,'max_value':max_value})
        if q.question_type in ['S']:
            quests[count]['choice']=VOTE
        if q.question_type in ['M','A']:
            quests[count]['choices']= q.multiplechoice_set.all()
        if q.question_type in ['T','A']:
            quests[count]['text']='S'
        count += 1
    return render(request,'questions.html',{'questions':quests,'person':p})


@csrf_exempt
def poll(request,person):
    polls = Pool.objects.filter(active=True)
    if not polls:
        return redirect('/')
    return poll_html(polls[0],request,person)

def thanks(request):
    return render(request,'grato.html')

@csrf_exempt
def poll_id(request,person,p):
    polls = Pool.objects.filter(active=True, id=p)
    if not polls:
        return redirect('/grato')
    poll=polls[0]
    return poll_html(poll,request,person)

     
    


def person(request):
    form = PersonForm()
    if request.method == 'POST':
        form = PersonForm(request.POST)
        #form.data['birth']=parse(form.data['birth'])
        #print(form.data['birth'])
        #form.save()
        if form.is_valid():
            form.save()
            p=Person.objects.filter(email=form.data['email'])
            if p:

                return redirect('/pesquisa/'+str(p[0].id))
        else:
            p=Person.objects.filter(Q(email=form.data['email']) | Q(cell=form.data['cell']))
            if p:
                return redirect('/pesquisa/'+str(p[0].id))
            print form.errors
            return render(request, 'create_person.html', {'form': form})
    else:

        return render(request, 'create_person.html', {'form': form})

def index(request):
    return render(request, 'index.html')


# Create your views here.
