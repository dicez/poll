# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from models import Choice, Pool, Question, Person, MultipleChoice
from django.contrib import admin

admin.site.register(Pool)
admin.site.register(Choice)
admin.site.register(Question)
admin.site.register(Person)
admin.site.register(MultipleChoice)

# Register your models here.
