# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-27 04:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('choice', models.IntegerField(choices=[(-5, '-5'), (-4, '-4'), (-3, '-3'), (-2, '-2'), (-1, '-1'), (0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')], default=0, null=True)),
                ('free_text', models.CharField(max_length=2000, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('cell', models.CharField(max_length=200, null=True)),
                ('birth', models.DateTimeField(null=True)),
                ('transport', models.CharField(choices=[('T', 'Transporte p\xfablico'), ('U', 'T\xe1xi, Uber ou similares'), ('B', 'Bicicleta'), ('V', 'Ve\xedculo pr\xf3prio'), ('N', 'N\xe3o utilizo ve\xedculos')], max_length=1)),
                ('accept_spam', models.BooleanField(default=True)),
                ('accept_wzap', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pool',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('expire', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_text', models.CharField(max_length=200)),
                ('question_type', models.CharField(choices=[('S', 'Select'), ('T', 'Texto livre'), ('A', 'Ambos')], max_length=1)),
                ('poll', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pools.Pool')),
            ],
        ),
        migrations.AddField(
            model_name='choice',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pools.Person'),
        ),
        migrations.AddField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pools.Question'),
        ),
    ]
