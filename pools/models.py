# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

TRANSPORT = (
            ('T','Transporte público'),
            ('U','Táxi, Uber ou similares'),
            ('B','Bicicleta'),
            ('V','Veículo próprio'),
            ('N','Não utilizo veículos'),
            )
VOTE = (
        (-5,'-5'),
        (-4,'-4'),
        (-3,'-3'),
        (-2,'-2'),
        (-1,'-1'),
        (0,'0'),
        (1,'1'),
        (2,'2'),
        (3,'3'),
        (4,'4'),
        (5,'5'),
        )
TYPE= (
        ('S','Select'),
        ('T', 'Texto livre'),
        ('M', 'Multipla Escolha'),
        ('A', 'Ambos'),
    )

class Pool(models.Model):

    name = models.CharField(max_length=200)
    expire = models.DateField(null=True)
    active = models.BooleanField(default=True)
    def __unicode__(self):
        return self.name


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    poll = models.ForeignKey(Pool)
    question_type = models.CharField(max_length=1,choices=TYPE)
    min_value= models.CharField(max_length=200, default=None,blank=True, null=True)
    max_value= models.CharField(max_length=200, default=None,blank=True, null=True)
    def __unicode__(self):
        return self.poll.name +" - " + self.question_text

class MultipleChoice(models.Model):
    question = models.ForeignKey(Question)
    choice = models.CharField(max_length=200)
    def __unicode__(self):
        return self.question.poll.name + " - " + self.question.question_text +" - "+ self.choice
class Person(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    cell = models.CharField(max_length=200, null=True,blank=True, unique=True)
    birth = models.CharField(max_length=20,null=True,blank=True)
    transport = models.CharField(max_length=1,choices=TRANSPORT)
    accept_spam = models.BooleanField(default=True)
    accept_wzap = models.BooleanField(default=True)
    def __unicode__(self):
        return self.name + " - " + self.email



class Choice(models.Model):
    choice = models.IntegerField(default=0,choices=VOTE, null=True,blank=True)
    free_text = models.CharField(max_length=2000, null=True,blank=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    multiple = models.TextField(null=True)

    def create(self,question,person, choice=None,free_text=None,multiple=None):
        self.person=person
        self.question=question
        self.choice=choice
        self.free_text=free_text
        self.multiple=multiple
        self.save()

 