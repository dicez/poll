"""pool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from pools import views 
import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^$', views.index, name='home'),
    url(r'^cadastro/', views.person,name='cadastro' ),
    url(r'^grato/', views.thanks,name='grato' ),
    url(r'^pesquisa/(?P<person>[\w\-]+)/$', views.poll,name='pesquisa' ),
    url(r'^pesquisa/(?P<person>[\w\-]+)/(?P<p>[\w\-]+)/$', views.poll_id,name='pesquisa' ),
    url(r'^$', views.index, name='pesquisa'),
] + static(settings.STATIC_URL)

